class NpGallery {

  selector: HTMLElement;
  settings: any;
  defaults: any = {
    classes: {
      active: 'is-active',
      hidden: 'is-hidden'
    }
  };
  items: any = {
    current: Number,
    amount: Number,
    nodes: NodeList
  }

  constructor(selector, options = {}) {
    this.selector = document.querySelector(selector);
    this.items.nodes = document.querySelectorAll('[data-np-gallery-item]');
    this.items.amount = this.items.nodes.length - 1;
    // @TODO: merge options and defaults
    this.settings = this.defaults;

    this.initGallery();
  }

  initGallery() {
    this.showElement(0);
    this.items.nodes.forEach((element: HTMLElement, iterator) => {
      element.addEventListener('animationend', () => {
        if (element.classList.contains(this.settings.classes.active)) {
          this.showElement(this.items.current + 1);
        }
      });
    });
  }

  showElement(index: Number) {
    index = index <= this.items.amount ? index : 0;
    this.items.current = index;

    this.items.nodes.forEach((element: HTMLElement, iterator) => {
      if (index === iterator) {
        element.classList.add(this.settings.classes.active);
        element.classList.remove(this.settings.classes.hidden);
      } else {
        element.classList.add(this.settings.classes.hidden);
        element.classList.remove(this.settings.classes.active);
      }
    });

  }

}

document.addEventListener("DOMContentLoaded", function (event) {
  new NpGallery('[data-np-gallery]', {});
});